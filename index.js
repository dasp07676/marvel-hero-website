// Urls
const comicUrl = 'https://gateway.marvel.com/v1/public/characters/id/comics?ts=1&apikey=9ef5becac14c29a5a812d841276849ef&hash=91188c3159f750a94453a28c5324656b';
const searchCharacter = 'https://gateway.marvel.com/v1/public/characters?ts=1&apikey=9ef5becac14c29a5a812d841276849ef&hash=91188c3159f750a94453a28c5324656b&name=';
const url = 'https://gateway.marvel.com/v1/public/characters?ts=1&apikey=9ef5becac14c29a5a812d841276849ef&hash=91188c3159f750a94453a28c5324656b';

// QuerySelectors
const inputEl = document.querySelector('.input');
const submitEl = document.querySelector('.submit');
const csEl = document.querySelector('.characters-section');
const comicsectionEl = document.querySelector('.comics-section');
const comicdetailEl = document.querySelector('.comic-detail');
const comicInnerEl = document.querySelector('.comic-inner');
const thEl = document.querySelector('.Trending-Heroes');
const endEl = document.querySelector('.end');

// Gives the first page of the website
getTrendingHeroes(url);

async function getTrendingHeroes(url) {
	const resp = await fetch(url);
	const respData = await resp.json();
	console.log(respData);
	allTrendingHeroes(respData.data.results); //Fetching the values from Api and passing those values to create a single hero card. 
}

function allTrendingHeroes(heroes) {
	const heroHeaderEl = document.createElement('div');
	heroHeaderEl.classList.add('hero-header');
	heroHeaderEl.innerHTML = "Trending characters in the Universe:"
	thEl.appendChild(heroHeaderEl);

	heroes.forEach((hero) => {
		const thumbnail = (hero.thumbnail.path === 'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available') ? 'https://image.shutterstock.com/shutterstock/photos/1882317979/display_1500/stock-vector-grunge-green-not-available-word-round-rubber-seal-stamp-on-white-background-1882317979' : hero.thumbnail.path;
		const type = hero.thumbnail.extension;
		const img = thumbnail + "." + type;
		const name = hero.name;
		const description = hero.description;
		const id = hero.id;

		const trendingEl = document.createElement("div");
		trendingEl.classList.add("append-hero");

		if (img) {
			trendingEl.innerHTML += `
				<img src="${img}" class="append-heroImg">
				<div class="hero-info">
					<h3>${name}</h3>
				</div>
			`;
		}

		thEl.appendChild(trendingEl); //Adding the dynamically created hero card to the Trending Hero class

		//If clicked on a single hero card
		trendingEl.addEventListener('click', () => {
			const urlByName = `https://gateway.marvel.com/v1/public/characters?ts=1&apikey=9ef5becac14c29a5a812d841276849ef&hash=91188c3159f750a94453a28c5324656b&name=${name}`;
			getCharacter(urlByName); //After fetching the data from Api we are passing the data through the next function which takes only the name of a hero
			thEl.innerHTML = '';
		});
	});
}

async function getCharacter(url) {
	const resp = await fetch(url);
	const respData = await resp.json(); //After fetching a single hero name it parses the data by JSON and
	searchHero(respData.data.results); //Passes that hero's data to the next step
}

submitEl.onclick = function (e) { //If any value is put on the input field
	e.preventDefault();
	const searchTerm = inputEl.value;

	if (searchTerm) {
		getCharacter(searchCharacter + searchTerm); //Adding the name of the hero with the Search Api
		inputEl.value = '';
		thEl.innerHTML = '';
		csEl.innerHTML = '';
		comicsectionEl.innerHTML = '';
		comicInnerEl.innerHTML = '';
	}
}

function searchHero(characters) { 						   //Creating a single hero card and then appending it by for loop until it reaches the length of the characters, where character is nothing but the data which is fetched from the Api	
	const characterHeader = document.createElement('div');
	characterHeader.classList.add('character-header');
	characterHeader.innerHTML = "Character's details:"
	csEl.appendChild(characterHeader);

	characters.forEach((character) => {
		const thumbnail = (character.thumbnail.path === 'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available') ? 'https://image.shutterstock.com/shutterstock/photos/1882317979/display_1500/stock-vector-grunge-green-not-available-word-round-rubber-seal-stamp-on-white-background-1882317979' : character.thumbnail.path;
		const type = character.thumbnail.extension;
		const img = thumbnail + "." + type;
		const name = character.name;
		const description = (character.description === '') ? 'Not given' : character.description;  	//Sometimes Api does't provide hero's description.Which means they don't have those hero's description.Such as Black Widow.
		const id = character.id;  //Getting the value of the id of the hero

		const characterEl = document.createElement("div");
		characterEl.classList.add("character");

		if (img) {
			characterEl.innerHTML = `
				<div class="hero-section">
					<div class="hero">
						<img src="${img}"/>
					</div>
					<div class="description">		
						<h1>${name}</h1><br>
						<p>${description}</p>
					</div>
				</div>
			`;
		}
		csEl.appendChild(characterEl);
		getComics(id); //Passing it to the function to create comic-section
	});
}

function getComics(id) {
	const comicUrl1 = 'https://gateway.marvel.com/v1/public/characters/';
	const characterId = id;
	const comicUrl2 = '/comics?ts=1&apikey=9ef5becac14c29a5a812d841276849ef&hash=91188c3159f750a94453a28c5324656b';
	const comicUrl = comicUrl1 + characterId + comicUrl2;

	fetch(comicUrl)					//Fetching the data of a single hero's comics
		.then(resp => resp.json())
		.then(data => {
			console.log(data.data.results);
			searchComics(data.data.results);
		});
}

function searchComics(comics) {		//Searching those comics
	const header = document.createElement('div');
	header.classList.add('header');
	header.innerHTML = "Featured Comics:";

	comicsectionEl.classList.add('comic-sectionReal')

	comicsectionEl.appendChild(header);

	for (let x = 0; x < 5; x++) {	//There could be more than 10 comics and their creators,so I took only 5.Sometimes it gives an error because the Api doesn't provide the all data we want.So lets compromise!
		let img = '';
		let comicdescription = '';
		let comictitle = '';
		let comicCreatorsName = '';
		let comicCreatorsRole = '';

		if (comics[x] === undefined) {
			img = 'https://image.shutterstock.com/shutterstock/photos/1882317979/display_1500/stock-vector-grunge-green-not-available-word-round-rubber-seal-stamp-on-white-background-1882317979.jpg';
			comicdescription = 'Not given';
			comictitle = 'Not given';
			comicCreatorsName = 'Not given';
			comicCreatorsRole = 'Not given';
		} else {
			img = comics[x].thumbnail.path;
			comicdescription = (comics[x].description === null) ? 'Not given' : comics[x].description;
			comictitle = (comics[x].title === '') ? 'Not given' : comics[x].title;

			if (comics[x].creators.items[x] === undefined) {
				comicCreatorsName = 'Not given';
				comicCreatorsRole = 'Not given';
			} else {
				comicCreatorsName = comics[x].creators.items[x].name;
				comicCreatorsRole = comics[x].creators.items[x].role;
			}
		}
		const comicImg = img + '.jpg';
		const comicEl = document.createElement("div");
		comicEl.classList.add("comic");

		if (comicImg) {
			comicEl.innerHTML = `
				<div class="comic-section">
					<img src="${comicImg}" class="comicImg"/>
				</div>
				<h2>${comictitle}</h2>
			`;
		}

		comicsectionEl.appendChild(comicEl);

		comicEl.addEventListener('click', () => {				//If any comic is clicked

			thEl.innerHTML = '';
			csEl.innerHTML = '';
			comicsectionEl.innerHTML = '';
			comicdetailEl.innerHTML = '';

			const innerEl = document.createElement('div');
			innerEl.classList.add('details');

			const comicDetailHeader = document.createElement('div');
			comicDetailHeader.classList.add('comic-detail-header');
			comicDetailHeader.innerHTML = "Comic details:";
			comicInnerEl.appendChild(comicDetailHeader);

			comicdetailEl.classList.add('comic-detail-real');

			innerEl.innerHTML += `
				<div class="ci">
					<img src="${comicImg}" class="comicimg">
				</div>
				<div class="cs">
					<div class="ct"><b>Title: </b>${comictitle}</div>
					<div class="cd"><b>Description: </b>${comicdescription}</div>
					<div class="nr"><b>Comic creator: </b>${comicCreatorsName} &nbsp&nbsp&nbsp <b>Role: </b>${comicCreatorsRole}</div>
				</div>
			`;
			comicInnerEl.appendChild(innerEl);
		});
	}
}